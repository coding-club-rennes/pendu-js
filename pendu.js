$(document).ready(function()
{
  /*
    Déclarez les variables dont vous aurez besoin ici.
    Vous en aurez besoin pour stocker le nombre d'essais, le mot à trouver, etc...
  */

  // 1 : Trouvez comment cacher la classe "j2". (voir .hide() sur le pdf)

    $("#validate_word").click(function(){ // Cette fonction est appelée lorsque le premier joueur a appuyé sur le bouton "Valider"

    // 2 : Il faut stocker le contenu de l'input ayant l'id "word" dans une variable. (voir .val() sur le pdf)
    // 3 : Cachez la classe "j1" et faites apparaître la classe j2. (voit .hide() et .show() sur le pdf)
  });

  $("#validate_answer").click(function(){ // Cette fonction est appelée lorsque le deuxième joueur a appuyé sur le bouton "Valider"

    // 4 : Stockez la valeur de l'input du deuxième joueur. (voir .val() sur le pdf)

    // 5 : vérifiez que la longueur de l'input est bien égale à 1. (voir .length sur le pdf)

    // 6 : Si la longueur est égale à 1, il faut vérifier que cette lettre n'a pas déjà été utilisée (voir indexOf() sur le pdf)

    // 8 : Si elle n'est pas utilisée, il faut la sauvegarder dans une chaîne de caractère. (chaine += le caractère)

    // 9 : il faut maintenant vérifier que la lettre se trouve bien dans chaîne entrée par le joueur 1. (voir indexOf() sur le pdf)

    // 10 : Si non, le nombre d'essais restant doit diminuer.

    // 11 : Si le nombre d'essais restant est à 0, faites apparaître une alerte signalant que le joueur a perdu. (voir alert() sur le pdf)

    // 12 : Affichez les éléments de jeu : voir (.val(variable) sur le pdf)
    // -Le nombre d'essais restant dans l'input "essais"
    // -Les lettres essayées dans l'input "lettres_essais"

    // 13 : Plus difficile, il faut afficher le mot avec les lettres trouvées. (b_n__ur) -> bonjour lorsqu'on a trouvé les lettres b, n, u et r. (Je vous laisse chercher ;) )
  });
});
